from tkinter import *
from PIL import Image, ImageTk
from main import loginregist
from main import BankAccount
from main import MinimumBalanceAccount


class AtmMachine:
    def __init__(self):
        self.__ButtonSet = []
        self.__LabelSet = ["Empty","Empty","Empty","Empty","Empty","Empty"]
        # Coordinate x,y for label and button
        self.__button_Coordinate_X = [50,50,50,810,810,810]
        self.__button_Coordinate_Y = [150,280,400,150,280,400]
        self.__label_Coordinate_X = [270,270,270,700,700,700]
        self.__label_Coordinate_Y = [200,330,450,200,330,450]

        self.__window = Tk()
        self.__window.title("VIRTUAL ATM MACHINE")

        # Button
        pressl = Image.open('/Users/pasutapaopun/Desktop/PyProject/images/poom.png')
        pressr = Image.open('/Users/pasutapaopun/Desktop/PyProject/images/poom1.png')
        self.__photo_pressl = ImageTk.PhotoImage(pressl)
        self.__photo_pressr = ImageTk.PhotoImage(pressr)

        # Load background image.
        self.__image = Image.open('/Users/pasutapaopun/Desktop/PyProject/images/atm.png')
        self.__photo_image = ImageTk.PhotoImage(self.__image)
        self.__label = Label(self.__window, image=self.__photo_image)
        self.__label.pack()

        #init account
        self.ath = loginregist()
        self.bankAccount = BankAccount()

    def bg(self):
        # Clear background
        self.__image = Image.open('/Users/pasutapaopun/Desktop/PyProject/images/atm.png')
        self.__photo_image = ImageTk.PhotoImage(self.__image)
        self.__label = Label(self.__window, image=self.__photo_image)
        self.__label.place(x = 0, y = 0)


    def createButton1(self,index,comm):
        # Create Button
        position_X = self.__button_Coordinate_X[index]
        position_Y = self.__button_Coordinate_Y[index]
        if self.__button_Coordinate_X[index] == 50:
            button = Button(self.__window, image=self.__photo_pressl,command = comm)
            button.place(x=self.__button_Coordinate_X[index], y=self.__button_Coordinate_Y[index])
        elif self.__button_Coordinate_X[index] == 810:
            button = Button(self.__window, image=self.__photo_pressr, command = comm)
            button.place(x=self.__button_Coordinate_X[index], y=self.__button_Coordinate_Y[index])
        return button


    def createLabelAt(self, index, textt):
        # Create Label
        position_X = self.__label_Coordinate_X[index]
        position_Y = self.__label_Coordinate_Y[index]

        if(self.__LabelSet[index] == "Empty"):
            self.__NewLabel = Label(self.__window, text = textt, font = 'VCROSDMono',bg = 'gray62',justify = 'left')
            self.__NewLabel.place(x=position_X, y=position_Y)


    ####FUNCTION COMMAND

    def passes(self):
        # Create non-function button
        pass


    def deposit(self):
        # Deposit
        self.bg()
        self.buttonn(self.passes, self.passes,self.deposit_calculate, self.passes, self.back)
        self.createLabelAt(2, 'OK')
        self.createLabelAt(4, 'BACK')
        Label(self.__window, text="Please enter amount ", font='VCROSDMono', bg='gray62',
                        justify='center').place(x=430, y=200)
        self.amount = StringVar()
        Entry(self.__window, textvariable = self.amount, bg='gray62', justify='center', width=67, borderwidth=0,
        font='VCROSDMono', highlightthickness=0).place(x=251, y=370)


    def deposit_calculate(self):
        self.get_amount = int(self.amount.get())
        balance_dep = self.bankAccount.deposit(self.get_amount)
        self.balanceee(balance_dep)
        self.bankAccount.update_bal()


    def withdraw(self):
        # Withdraw
        self.bg()
        self.buttonn(self.passes, self.passes, self.withdraw_calculate, self.passes, self.back)
        self.createLabelAt(2, 'OK')
        self.createLabelAt(4, 'BACK')
        Label(self.__window, text="Please enter amount ", font='VCROSDMono', bg='gray62',
        justify='center').place(x=430, y=200)
        self.amount = StringVar()
        Entry(self.__window, textvariable=self.amount, bg='gray62', justify='center', width=67, borderwidth=0,
              font='VCROSDMono', highlightthickness=0).place(x=251, y=370)


    def withdraw_calculate(self):
        self.bg()
        self.get_amount = int(self.amount.get())
        minimum = MinimumBalanceAccount(self.bankAccount.get_balance())
        if minimum.withdraw(self.get_amount):
            balance_wd = self.bankAccount.withdraw(self.get_amount)
            self.buttonn(self.passes, self.passes, self.back, self.passes, self.passes)
            self.createLabelAt(2, 'OK')
            self.balanceee(balance_wd)
            self.bankAccount.update_bal()
        else:
            Label(self.__window, text="Your money is not enough!", font='VCROSDMono',
                  bg='gray62', justify='center').place(x=430, y=300)
            self.buttonn(self.passes, self.passes,self.back , self.passes, self.passes)
            self.createLabelAt(2, 'BACK')



    def balanceee(self,balance_amount):
        # Balance
        self.bg()
        accbala = Label(self.__window, text = "Your account balance is ", font='VCROSDMono', bg='gray62', justify='center')
        accbala.place(x = 430, y = 200)
        balancee = Label(self.__window, text = balance_amount, font='VCROSDMono', bg='gray62', justify='center')
        balancee.place(x = 500,y = 300)
        self.buttonn(self.passes,self.passes,self.back,self.passes,self.passes)
        self.createLabelAt(2,'OK')

    def get_balance(self):
        self.bg()
        accbala = Label(self.__window, text="Your account balance is ", font='VCROSDMono', bg='gray62',
                        justify='center')
        accbala.place(x=430, y=200)
        balancee = Label(self.__window, text=self.bankAccount.get_balance(), font='VCROSDMono', bg='gray62', justify='center')
        balancee.place(x=500, y=300)
        self.buttonn(self.passes, self.passes, self.back, self.passes, self.passes)
        self.createLabelAt(2, 'OK')

    def buttonn(self,c0,c1,c2,c3,c4):
        self.createButton1(0, c0)
        self.createButton1(1, c1)
        self.createButton1(2, c2)
        self.createButton1(3, c3)
        self.createButton1(4, c4)
        self.createLabelAt(5, "EXIT")
        self.createButton1(5, exit)

    def labell(self,label_index, label_text):
        for i in range(len(label_index)):
            self.createLabelAt(label_index[i], label_text[i])


    def menu_screen(self):
        startmenu_label_index = [0, 1, 2]
        startmenu_label_text = ["CHECK BALANCE", "DEPOSIT CASH", "WITHDRAW CASH"]
        self.buttonn(self.get_balance,self.deposit,self.withdraw,self.passes,self.passes)
        self.labell(startmenu_label_index, startmenu_label_text)

    def login_screen(self):
        self.bg()
        self.username = StringVar()
        self.password = StringVar()
        Label(self.__window, text= 'Enter username' , font='VCROSDMono', bg='gray62', justify='center').place(x = 450, y = 230)
        Entry(self.__window, textvariable=self.username, bg='gray62', justify='center', width=67, borderwidth=0,
              font='VCROSDMono', highlightthickness=0).place(x=251, y=280)

        Label(self.__window, text='Enter password', font='VCROSDMono', bg='gray62', justify='center').place(x=450,y=320)
        Entry(self.__window, textvariable=self.password, bg='gray62', justify='center', width=67, borderwidth=0,
              font='VCROSDMono', highlightthickness=0).place(x=251, y=370)

        self.buttonn(self.passes,self.passes,self.get_account, self.passes, self.start_screen)
        self.createLabelAt(2, 'OK')
        self.createLabelAt(4, 'BACK')



    def get_account(self):
        self.get_username_login = str(self.username.get())
        self.get_password_login = str(self.password.get())

        self.bg()

        if self.ath.login(self.get_username_login, self.get_password_login):
            self.bankAccount = BankAccount(self.get_username_login)
            Label(self.__window, text="Welcome!", font='VCROSDMono',
                  bg='gray62', justify='center').place(x=500, y=300)
            self.buttonn(self.passes, self.passes, self.back, self.passes, self.passes)
            self.createLabelAt(2, 'YEAHHHH')

        else:
            Label(self.__window, text="Wrong Username and Password!", font='VCROSDMono',
                  bg='gray62', justify='center').place(x=430, y=300)
            self.buttonn(self.passes, self.passes, self.login_screen, self.passes, self.passes)
            self.createLabelAt(2, 'TRY AGAIN')

    def start_screen(self):
        self.bg()
        Label(self.__window, text="Access new bank account", font='VCROSDMono',
                  bg='gray62', justify='center').place(x=270, y=200)
        Label(self.__window, text="Use existing account", font='VCROSDMono',
                  bg='gray62', justify='center').place(x=270, y=330)
        self.buttonn( self.register_screen, self.login_screen, self.passes, self.passes, self.passes)

    def register_screen(self):
        self.bg()
        self.username = StringVar()
        self.password = StringVar()
        Label(self.__window, text='Enter username', font='VCROSDMono', bg='gray62', justify='center').place(x=450,
                                                                                                            y=230)
        Entry(self.__window, textvariable=self.username, bg='gray62', justify='center', width=67, borderwidth=0,
              font='VCROSDMono', highlightthickness=0).place(x=251, y=280)

        Label(self.__window, text='Enter password', font='VCROSDMono', bg='gray62', justify='center').place(x=450,
                                                                                                            y=320)
        Entry(self.__window, textvariable=self.password, bg='gray62', justify='center', width=67, borderwidth=0,
              font='VCROSDMono', highlightthickness=0).place(x=251, y=370)

        self.buttonn(self.passes, self.passes, self.register, self.passes, self.start_screen)
        self.createLabelAt(2, 'OK')
        self.createLabelAt(4, 'BACK')

    def register(self):
        self.get_username_regist = str(self.username.get())
        self.get_password_regist = str(self.password.get())
        regist = loginregist()
        regist.signup(self.get_username_regist,self.get_password_regist)
        self.bg()
        Label(self.__window, text="Welcome! \nYour starter balance is 5000 ฿", font='VCROSDMono',
              bg='gray62', justify='center').place(x=400, y=300)
        self.buttonn(self.passes, self.passes, self.back, self.passes, self.passes)
        self.createLabelAt(2, 'YEAHHHH')

    def back(self):
        self.bg()
        self.menu_screen()


    def show(self):
        self.__window.mainloop()


a = AtmMachine()
a.start_screen()
a.show()
