class loginregist:
    def login(self, username, password):
        login_list = [get_user.rstrip('\n') for get_user in open("account.txt")]
        login_check = str(username) + " " + str(password)

        if login_check in login_list:
            valid = True
        else:
            valid = False

        return valid

    def signup(self,username,password):
        outfile = open("account.txt", "a")
        outfile.write('\n' + username + ' ' + password)
        outfile.close()


class BankAccount(object):
    def __init__(self,name=""):
        self.name = name
        self.balance = 5000
        try :
            file = open("Balance.csv","r")
        except:
            file = open("Balance.csv","w+")
        if file :
            for key in file:
                sub = key.split(',')
                if sub[0] == self.name:
                    self.name = sub[0]
                    self.balance = int(sub[1])

    def withdraw(self, amount):
        self.balance -= amount
        return self.balance

    def deposit(self,amount):
        self.balance += amount
        return self.balance

    def balance(self):
        loginregist.login


    def update_bal(self):
        pre_list = []
        infile = open("Balance.csv","r")
        if infile :
            for i in infile:
                some = []
                sub = str(i).split(",")
                some.append(sub[0])
                some.append(sub[1])
                pre_list.append(some)
            for k in  pre_list:
                if k[0] == self.name:
                    k[1] = self.balance
                    infile.close()
                    outfile = open("Balance.csv", "w")
                    for j in pre_list:
                        outfile.write(str(j[0]) + "," + str(j[1]) )
                        outfile.close()
                        return

        sub2 = []
        sub2.append(self.name)
        sub2.append(self.balance)
        pre_list.append(sub2)
        infile.close()
        outfile =open("Balance.csv","w")
        for j in pre_list:
            outfile.write(str(j[0])+","+str(j[1]))
        outfile.close()

    def get_balance(self):
        return self.balance

    def exit(self):
        exit(0)

class MinimumBalanceAccount(BankAccount):
    # Inheritance
    def __init__(self, minimum_balance):
        BankAccount.__init__(self)
        self.minimum_balance = minimum_balance

    def withdraw(self, amount):
        if amount > self.minimum_balance:
            valid = False
        else:
            valid  = True

        return valid


